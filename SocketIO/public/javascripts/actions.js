      var socket = io();
      $('form').submit(function(){
		var text = $('#m').val();
		$('#messages').append($('<li>').text('I: ' + text).css('color', '#595959'));
		socket.emit('chat message', text);
		$('#m').val('');
		return false;
      });
      socket.on('chat message', function(msg){
        $('#messages').append($('<li>').text(msg).css('color', '#408080'));
      });
      socket.on('current name', function(msg){
        $("#header").html('<H1>'+msg+'</H1>');
      });
      socket.on('add user', function(object){
        $('#messages').append($('<li>').text(object.message).css('color', '#158000'));
      });
      socket.on('delete user', function(object){/*to use 'append '<span style='color1'>' '*/
        $('#messages').append($('<li>').text(object.message).css('color', '#FF0000'));
      });
	  function setNickname() {
		 socket.emit('add user', $('#username').val());
      }
	  function getAllUsers() {
		 socket.emit('get all users', $('#username').val());
      }