// Startup Express App
var express = require('express');
var app = express();
var mysql = require('mysql');
var http = require('http').Server(app);
var io = require('socket.io')(http);
http.listen(process.env.PORT || 3000);

// handle HTTP GET request to the "/" URL
app.use(express.static(__dirname + '/public'));
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

//necessary variables
var users = new Object();
var numUsers = 0;
var questId = 0;

var pool      =    mysql.createPool({
    connectionLimit : 3, //important
    host     : 'us-cdbr-iron-east-04.cleardb.net',
    user     : 'bd39196afcc8e6',
    password : 'a87f1cbe',
    database : 'ad_92cb634dbc6f4dd',
    debug    :  false
});

// socket.io listen for messages
io.on('connection', function(socket){
  console.log('a user connected with socket ' + socket.id);
  socket.username = "Guest"+questId;
  ++numUsers;
  ++questId;
  users[socket.id] = socket;
	var msg = socket.username+" joined; Count users online: "+numUsers;
    socket.broadcast.emit('add user', {
      message: msg
    });

  //select history
    pool.getConnection(function(err,connection){
        if (err) {
          console.log("code: 100; status: Error in connection database");
          return;
        } 
        var res= connection.query("select h.* from (select * from history order by id desc limit 50) as h order by h.id",function(err,rows,cols){
            connection.release();
            if(!err) {
				//разделения по событиям нужно, т.к. разное оформление, да и вообще легко управлять, можно просто и сообщения оставить
				for(var i in rows) {
					//console.log(rows[i].text);
					if (rows[i].event == "in") {
						socket.emit('add user', { message: rows[i].text })
					} else if (rows[i].event == "message") {
						socket.emit('chat message', rows[i].text)
					} else if (rows[i].event == "out") {
						socket.emit('delete user', { message: rows[i].text })
					}
				}
            }          
        });
        connection.on('error', function(err) {      
              console.log("code: 100; status: Error in connection database");
              return;    
        });
  });
  //end select history

  //fix add
    pool.getConnection(function(err,connection){
        if (err) {
          console.log("code: 100; status: Error in connection database");
          return;
        }
        connection.query("INSERT INTO history SET ?",{text: msg, event: "in"},function(err,recordId){
            connection.release();
            if(!err) {
                console.log(recordId);
            }          
        });
        connection.on('error', function(err) {      
              console.log("code: 100; status: Error in connection database");
              return;    
        });
  });
  //end fix add
 
	socket.emit('current name', 'Hello, '+socket.username)
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
	var message = socket.username+": "+msg;
  //fix message
    pool.getConnection(function(err,connection){
        if (err) {
          console.log("code: 100; status: Error in connection database");
          return;
        }
        connection.query("INSERT INTO history SET ?",{text: message, event: "message"},function(err,recordId){
            connection.release();
            if(!err) {
                console.log(recordId);
            }          
        });
        connection.on('error', function(err) {      
              console.log("code: 100; status: Error in connection database");
              return;    
        });
  });
  //end fix message
    socket.broadcast.emit('chat message', message);	
  });
  socket.on('get all users', function(msg){
	var concatNames = "";
	for(var key in users){
   		console.log('key: ' + key + ' username: '+ users[key].username);
		if (key != socket.id) {
			concatNames += users[key].username + ";";
		}
	}
	socket.emit('chat message', 'usernames in room: '+concatNames);
  });
  socket.on('add user', function(username){
	var socketId = 0;
	for(var key in users){
		if (users[key].username == username) {
			socketId = users[key].id;
		}
	}
	if (socketId == 0) {
		var oldname = socket.username;
    	socket.username = username;
		users[socket.id] = socket;
		var message = oldname +' change name on '+socket.username;
  //fix message
    pool.getConnection(function(err,connection){
        if (err) {
          console.log("code: 100; status: Error in connection database");
          return;
        }
        connection.query("INSERT INTO history SET ?",{text: message, event: "message"},function(err,recordId){
            connection.release();
            if(!err) {
                console.log(recordId);
            }          
        });
        connection.on('error', function(err) {      
              console.log("code: 100; status: Error in connection database");
              return;    
        });
  });
  //end fix message
		
    	socket.broadcast.emit('chat message', message)
		socket.emit('current name', 'Hello, '+socket.username)
	} else {
		if (socket.id == socketId) {
			socket.emit('chat message', 'You already has this nickname!')	
		} else {
			socket.emit('chat message', 'This nickname already exist!')
		}
	}
 });
  socket.on('send to', function(msg){
	var user = msg.user;
	var message = msg.messag;
	var userSender = users[socket.id].username;
	for(var key in users){
		if (users[key].username == user && users[key].id != socket.id) {
			io.sockets.sockets[users[key].id].emit('chat message', userSender+'(personally): ' + message);
		}
	}
  });
  socket.on('disconnect', function() { 
        console.log(socket.username + ' disconnected');
	--numUsers;
	if (numUsers == 0) {
		questId = 0;
	} 
	 delete users[socket.id];

	var msg = socket.username+" go out; Count users online: "+numUsers;

  //fix delete
    pool.getConnection(function(err,connection){
        if (err) {
          console.log("code: 100; status: Error in connection database");
          return;
        }
        connection.query("INSERT INTO history SET ?",{text: msg, event: "out"},function(err,recordId){
            connection.release();
            if(!err) {
                console.log(recordId);
            }          
        });
        connection.on('error', function(err) {      
              console.log("code: 100; status: Error in connection database");
              return;    
        });
  });
  //end fix delete

     socket.broadcast.emit('delete user', {
    	message: msg
   	 });
  });
});