--id	text		event
--1	Quest 1 joined	in
--2	Quest 1: hello	message
--3	Quest 1 out	out
create database Chat;
use Chat;
create table history (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	text varchar(4000) not null,
	event varchar(7) not null
);
insert into history(text,event) VALUES ("Guest1 joined","in"),("Guest1 message","message"),("Guest1 out","out");